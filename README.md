<div>
    <a target='_blank' href="https://www.instagram.com/felipepsilveira/">
        <img src="https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white">
    </a>
    <a target='_blank' href="https://www.linkedin.com/in/felipe-silveira/">
        <img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white">
    </a>
    <a target='_blank' href="https://cadeolog.com.br">
        <img src="https://img.shields.io/badge/site-0A0A0A?style=for-the-badge&logo=dev.to&logoColor=white"">
    </a>
</div>

## Informações

* **Nome**: Felipe Pereira Silveira
* **Idade**: 24
* **Vivendo em**: São Paulo - SP, Brazil
* **Conhecimentos**: Cybersecurity - Linux - Windows - Azure - Redes
* **Graduação**: Centro Universitário Senac - Tecnologia em Redes de Computadores
* **Pós Graduação**: IPOG - Instituto de Pós-Graduação e Graduação - Computação Forense e Pericia Digital
* **Pós Graduação**: Centro de Inovação VincIT - UNICIV - Especialização - Segurança Defensiva – Blue Team Operations

## Áreas de interesse
                                                                                                            
* **Cyber Security** principalmente referente área de Blue Team e Purple Team
* **Automatização** de buscas maçantes utilizando scripts Shell(Bash) ou Powershell
* **Forense** computacional voltado para a área de resposta a incidente                              
                                                                               



